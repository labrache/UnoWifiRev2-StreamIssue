#include <SPI.h>
#include "Adafruit_VS1053.h" // <--- File from PR#68 https://github.com/adafruit/Adafruit_VS1053_Library/pull/68
#include <WiFiNINA.h>

#include "arduino_secrets.h"

//*********** WIFI
char ssid[] = SECRET_SSID;
char pass[] = SECRET_PASS;
WiFiClient client;

//*********** Debug
unsigned long lastConnectionTime = 0;
const unsigned long postingInterval = 500L;
long totalbytesread;

//*********** VS1053 SHIELD
#define SHIELD_RESET  -1
#define SHIELD_CS     7
#define SHIELD_DCS    6
#define DREQ 3
Adafruit_VS1053 musicPlayer = Adafruit_VS1053(SHIELD_RESET, SHIELD_CS, SHIELD_DCS, DREQ);
int volume = 70;

//*********** Streaming
String streamhost = "radiofg.impek.com";
String streampath = "/fgd.mp3";
#define BUFFSIZE 32
uint8_t abuffer[BUFFSIZE];


void setup() {
  Serial.begin(115200);
  while (!Serial) {
    ;
  }
  Serial.println(F("Init"));
  if (! musicPlayer.begin()) {
     Serial.println(F("Couldn't find VS1053, do you have the right pins defined?"));
     while (1);
  }
  Serial.println(F("VS1053_OK"));
  musicPlayer.setVolume(volume,volume);
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
  }
  Serial.println(F("WIFI_OK"));
  if (client.connect(streamhost.c_str(), 80)) {
    client.println("GET "+streampath+" HTTP/1.1");
    client.println("Host: "+streamhost);
    client.println("User-Agent: ArduinoWiFi/1.1");
    client.println("Connection: close");
    client.println();
    Serial.println("connect_OK");
  } else {
    Serial.println("FAIL");
  }
}

void loop() {
  if(client.available()) {
    totalbytesread = totalbytesread + client.available();
    if (musicPlayer.readyForData()) {
      uint8_t bytesread = client.read(abuffer, BUFFSIZE);
      musicPlayer.playData(abuffer, bytesread);
    }
  }
  
  if (millis() - lastConnectionTime > postingInterval) {
    Serial.println(totalbytesread);
    totalbytesread = 0;
    lastConnectionTime = millis();
  }
  
}
